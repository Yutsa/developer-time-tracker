import 'package:developer_time_tracker/viewmodel/work_day_duration_view_model.dart';
import 'package:developer_time_tracker/widgets/stopwatch_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockModel extends Mock implements WorkDayDurationViewModel {}

void main() {
  testWidgets("When creating the widget, the icon is the pause icon.",
          (WidgetTester tester) async {
        await createStopwatchButton(tester);

        final findPlayIcon = findStopwatchPlay();

        expect(findPlayIcon, findsOneWidget);
      });

  testWidgets("When clicking on the start button, the icon changes to pause.",
          (WidgetTester tester) async {
        await createStopwatchButton(tester);

        await tester.tap(findStopwatchPlay());

        final findPauseIcon = findStopwatchPause();
        await tester.pump();

        expect(findPauseIcon, findsOneWidget);
      });

  testWidgets("When clicking on the pause button, the icon changes to play.",
          (WidgetTester tester) async {
        await createStopwatchButton(tester);

        await tester.tap(findStopwatchPlay());

        final findPauseIcon = findStopwatchPause();
        await tester.pump();
        await tester.tap(findPauseIcon);
        await tester.pump();

        final findPlayIcon = findStopwatchPlay();

        expect(findPlayIcon, findsOneWidget);
      });
}

Future createStopwatchButton(WidgetTester tester) async {
  await tester.pumpWidget(Directionality(
    textDirection: TextDirection.ltr,
    child: StopwatchButton(MockModel()),
  ));
}

Finder findStopwatchPause() => find.widgetWithIcon(StopwatchButton, Icons.pause);

Finder findStopwatchPlay() => find.widgetWithIcon(StopwatchButton, Icons.play_arrow);


