import 'package:developer_time_tracker/viewmodel/work_day_duration_view_model.dart';
import 'package:developer_time_tracker/widgets/stopwatch_button.dart';
import 'package:developer_time_tracker/widgets/timer_display_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text("Developer Time Calculator"),
        ),
      ),
      body: Center(
        child: ChangeNotifierProvider(
          builder: (context) => WorkDayDurationViewModel(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Align(
                  alignment: Alignment.center,
                  child: Consumer<WorkDayDurationViewModel>(
                      builder: (context, model, child) => TimerDisplayWidget(model.timeWorked)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Builder(
                  builder: (context) {
                    final workDayDurationViewModel = Provider.of<WorkDayDurationViewModel>(context);
                    return StopwatchButton(workDayDurationViewModel);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
