
import 'package:developer_time_tracker/viewmodel/work_day_duration_view_model.dart';
import 'package:flutter/material.dart';

class StopwatchButton extends StatefulWidget {
  final WorkDayDurationViewModel _workDayDurationViewModel;

  @override
  _StopwatchButtonState createState() => _StopwatchButtonState(_workDayDurationViewModel);

  StopwatchButton(this._workDayDurationViewModel);
}

class _StopwatchButtonState extends State<StopwatchButton> {
  bool _isShowingPauseIcon = false;
  Icon _buttonIcon = _playIcon;
  WorkDayDurationViewModel _workDayDurationViewModel;

  static Icon _playIcon = Icon(Icons.play_arrow);
  static Icon _pauseIcon = Icon(Icons.pause);


  _StopwatchButtonState(this._workDayDurationViewModel);

  void _onClick() {
    setState(() {
      togglePlayPause();
    });
  }

  void togglePlayPause() {
    if (_isShowingPauseIcon) {
      _buttonIcon = _playIcon;
      _workDayDurationViewModel.pauseTimer();
    } else {
      _buttonIcon = _pauseIcon;
      _workDayDurationViewModel.startTimer();
    }

    _isShowingPauseIcon = !_isShowingPauseIcon;
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: _buttonIcon,
      onPressed: _onClick,
    );
  }
}