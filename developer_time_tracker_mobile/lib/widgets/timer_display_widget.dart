import 'package:flutter/material.dart';

class TimerDisplayWidget extends StatelessWidget {
  final String _duration;

  const TimerDisplayWidget(
      this._duration, {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      _duration,
      style: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
