import 'dart:async';

import 'package:developer_time_calculator/businessobjects/work_day.dart';
import 'package:developer_time_calculator/utils/duration_utils.dart';
import 'package:flutter/foundation.dart';


class WorkDayDurationViewModel extends ChangeNotifier {
  WorkDay _workDay;
  Timer _timer;

  WorkDayDurationViewModel() : _workDay = WorkDay(Stopwatch());

  String get timeWorked => formatDurationWithoutMiliseconds(_workDay.currentDuration);

  void startTimer() {
    _workDay.startCountingTime();
    _timer = Timer.periodic(Duration(seconds: 1), (_) => notifyListeners());
  }

  void pauseTimer() {
    _workDay.pauseTimer();
    _timer.cancel();
  }
}