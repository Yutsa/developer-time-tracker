import 'package:developer_time_calculator/services/working_duration_service.dart';
import 'package:developer_time_calculator/time/time_provider.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

final DateTime currentTime = DateTime.parse("2019-10-27");
WorkingDurationService _workingDurationService;
TimeProvider _timeProvider;
final Duration _workDayDuration = Duration(hours: 7, minutes: 24);

class FakeTimeProvider extends Fake implements TimeProvider {
  @override
  DateTime getCurrentTime() => currentTime;
}

void main() {
  setUp(() {
    _timeProvider = FakeTimeProvider();
    _workingDurationService = WorkingDurationService.withTimeProvider(
        _workDayDuration, _timeProvider);
  });

  group("Testing getEndOfWorkTime", () {
    checkEndOfWorkTime(
        "When current working duration is 0, returns now + workDayDuration",
        Duration(),
        currentTime.add(_workDayDuration));

    checkEndOfWorkTime(
        "When current working duration is > workDayDuration, returns the current time",
        _workDayDuration + Duration(hours: 2),
        currentTime);

    checkEndOfWorkTime(
        "When current working duration is = workDayDuration, returns the current time",
        _workDayDuration,
        currentTime);

    checkEndOfWorkTime(
        "When current working duration > 0 and < workDayDuration, returns now + time left to work",
        Duration(hours: 3, minutes: 24),
        currentTime.add(Duration(hours: 4)));

    checkEndOfWorkTimeWithException(
        "When current working duration is null, returns an exception",
        null,
        throwsArgumentError);
  });
}

void checkEndOfWorkTime(String description, Duration currentWorkingDuration,
    DateTime expectedResult) {
  test(description, () {
    expect(_workingDurationService.getEndOfWorkTime(currentWorkingDuration),
        expectedResult);
  });
}

void checkEndOfWorkTimeWithException(String description,
    Duration currentWorkingDuration, dynamic expectedResult) {
  test(description, () {
    expect(
        () => _workingDurationService.getEndOfWorkTime(currentWorkingDuration),
        expectedResult);
  });
}
