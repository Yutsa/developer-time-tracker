import 'package:developer_time_calculator/businessobjects/task.dart';
import 'package:developer_time_calculator/businessobjects/work_day.dart';
import 'package:test/test.dart';

void main() {
  test("workedDuration is an empty duration when creating a WorkDay", () {
    final WorkDay workDay = WorkDay(Stopwatch());
    expect(workDay.workedDuration, equals(Duration()));
  });

  test("When a new task is created from the work day, its timer should start", () {
    final WorkDay workDay = WorkDay(Stopwatch());
    final Task task = workDay.addNewTask("Task 1");
    expect(task.isRunning(), isTrue);
  });

  test("When an existing task is added to the work day, its timer should start", () {
    final WorkDay workDay = WorkDay(Stopwatch());
    final Task task = Task("Task 2");
    workDay.addExistingTask(task);
    expect(task.isRunning(), isTrue);
  });
}