String formatDurationWithoutMiliseconds(Duration duration) {
  String twoDigits(num n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  return "${twoDigits(duration.inHours)}:${twoDigits(duration.inMinutes.remainder(60))}:${twoDigits(duration.inSeconds.remainder(60))}";
}
