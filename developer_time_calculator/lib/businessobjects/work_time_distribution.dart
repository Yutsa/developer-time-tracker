import 'dart:collection';

import 'package:developer_time_calculator/businessobjects/task.dart';

class WorkTimeDistribution {
  final Map<Task, int> timeForTask;

  WorkTimeDistribution() : timeForTask = HashMap();

  void addTask(Task task, int time) => timeForTask[task] = time;
}