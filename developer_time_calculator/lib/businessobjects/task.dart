class Task {
  String label;
  final Stopwatch _stopwatch;

  Task(this.label) : _stopwatch = Stopwatch();

  Task.startRunning(this.label) : _stopwatch = Stopwatch() {
    _stopwatch.start();
  }

  bool isRunning() => _stopwatch.isRunning;

  void startCountingTime() => _stopwatch.start();
}
