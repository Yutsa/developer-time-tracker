import 'package:developer_time_calculator/businessobjects/task.dart';

class WorkDay {
  DateTime date;
  Set<Task> tasks;
  final Stopwatch _stopwatch;
  Duration get workedDuration => _stopwatch.elapsed;

  WorkDay(this._stopwatch)
      : date = DateTime.now(),
        tasks = {};

  void startCountingTime() => _stopwatch.start();

  void pauseTimer() => _stopwatch.stop();

  void stopCountingTime() => _stopwatch.stop();

  bool isRunning() => _stopwatch.isRunning;

  Duration get currentDuration => _stopwatch.elapsed;

  Task addNewTask(String taskLabel) {
    final Task task = Task.startRunning(taskLabel);
    tasks.add(task);
    return task;
  }

  void addExistingTask(Task task) {
    task.startCountingTime();
    tasks.add(task);
  }
}
