import 'package:developer_time_calculator/time/time_provider.dart';

class WorkingDurationService {
  Duration workDayDuration;
  final TimeProvider timeProvider;

  WorkingDurationService(this.workDayDuration) : timeProvider = TimeProvider();

  WorkingDurationService.withTimeProvider(this.workDayDuration, this.timeProvider);

  DateTime getEndOfWorkTime(Duration currentWorkingDuration) {
    if (currentWorkingDuration == null) {
      throw ArgumentError("The current working duration can't be null");
    }
    if (currentWorkingDuration >= workDayDuration) {
      return timeProvider.getCurrentTime();
    }
    return timeProvider.getCurrentTime().add(workDayDuration - currentWorkingDuration);
  }
}
